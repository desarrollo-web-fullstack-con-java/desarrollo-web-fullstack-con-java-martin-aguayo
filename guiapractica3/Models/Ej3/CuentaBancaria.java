/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guiapractica3.Models.Ej3;

/**
 *
 * @author marti
 */
public class CuentaBancaria {
    public int numero;
    public long dni;
    public double saldo;
    
    public CuentaBancaria(){}

    public CuentaBancaria(int numero, long dni, double saldo) {
        this.numero = numero;
        this.dni = dni;
        this.saldo = saldo;
    }
    
    
  
    // Setters
    public void setNumero(int numero) {
        this.numero = numero;
    }

    public void setDni(long dni) {
        this.dni = dni;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }
    
    // Getters 
    public int getNumero() {
        return numero;
    }

    public long getDni() {
        return dni;
    }

    public double getSaldo() {
        return saldo;
    }
    
    public void ingresar(double ingreso) {
        if (ingreso <= 0) {
           return;
        }
        saldo = saldo + ingreso;
    }
    
    
    public double extraccionRapida() {
        double valor = saldo * 0.2;
        saldo -= valor;
        return valor;
    }
    
    public double consultarSaldo() {
        return saldo;
    }
    
    public void consultarDatos() {
        System.out.println("Número de cuenta: " + numero);
        System.out.println("DNI del cliente: " + dni);
        System.out.println("Saldo actual: " + saldo);
    }
    
    
    
    

}
