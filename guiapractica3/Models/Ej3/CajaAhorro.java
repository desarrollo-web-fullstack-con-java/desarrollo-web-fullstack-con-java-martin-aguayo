/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guiapractica3.Models.Ej3;

/**
 *
 * @author marti
 */
public class CajaAhorro extends CuentaBancaria {

    public CajaAhorro() {
    }

    public CajaAhorro(int numero, long dni, double saldo) {
        super(numero, dni, saldo);
    }
     
    public double retirar(double retiro) {
        if(retiro > 0) {
            if(saldo >= retiro) {
               double valorRetirado = saldo - retiro;
               saldo -= retiro;
               return valorRetirado;
           }
        }
        return 0;
    }
}
