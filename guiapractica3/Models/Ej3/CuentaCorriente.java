/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guiapractica3.Models.Ej3;

/**
 *
 * @author marti
 */
public class CuentaCorriente extends CuentaBancaria{

    public CuentaCorriente() {
        super();
    }

    public CuentaCorriente(int numero, long dni, double saldo) {
        super(numero, dni, saldo);
    }
    
    public double retirar(double retiro) {
        if(retiro > 0) {
            double valorRetirado = Math.min(retiro, getSaldo());
            setSaldo(getSaldo() - valorRetirado);
            return valorRetirado;
           
        }
        return 0.0;
    }
    
}
