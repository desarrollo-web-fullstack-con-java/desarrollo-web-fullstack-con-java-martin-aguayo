package guiapractica3.Models.Ej2;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author marti
 */
public class Circunferencia {
    private double radio;

    public Circunferencia(double radio) {
        this.radio = radio;
    }

    public double getRadio() {
        return radio;
    }

    public void setRadio(double radio) {
        this.radio = radio;
    }

    public double calcArea() {
        return (3.14159265359 * (radio * radio));
    }
 
     public double calPerimetro() {
        return 2 * 3.14159265359 * radio;
    } 
}
