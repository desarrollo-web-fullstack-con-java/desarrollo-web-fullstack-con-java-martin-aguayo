/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guiapractica3;

import guiapractica3.Models.Ej3.CuentaCorriente;
import guiapractica3.Models.Ej3.CuentaBancaria;
import guiapractica3.Models.Ej2.Circunferencia;

/**
 *
 * @author marti
 */
public class GuiaPractica3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Circunferencia circunferencia = new Circunferencia(252.25);
        System.out.println(circunferencia.calcArea());
        
        CuentaBancaria cuenta = new CuentaBancaria(1, 41192241, 120000 );
        System.out.print("DNI cuenta: " + cuenta.dni + "\n" );
        System.out.print("Numero de cuenta: " + cuenta.numero + "\n");
        System.out.print("Saldo de la cuenta: " + cuenta.saldo + "\n");
        
        cuenta.consultarDatos();
        
        cuenta.consultarSaldo();
                
        cuenta.extraccionRapida();
        
        cuenta.ingresar(100000);
        
        CuentaCorriente cuentacorriente = new CuentaCorriente(2, 41192242, 10);
        cuentacorriente.retirar(3);
        cuentacorriente.consultarDatos();
        
    }
    
}
